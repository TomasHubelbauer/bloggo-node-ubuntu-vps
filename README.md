# NodeJS Ubuntu VPS

- [ ] Figure out how to enable IPv6 on NGinx and test it
- [ ] Add Postgres install instructions
- [ ] Introduce Yarn and fix generated `license` field to `MIT` with Yarn

> Tested on: Wedos, Digital Ocean

- Log in to the VPS: `ssh root@31.31.77.33`
- [Install NVM](https://github.com/creationix/nvm#install-script)
- Log in and out again: `logout` (to have `nvm` in `$PATH`)
- Install Node: `nvm install 9.4.0`
- Create the server app:
    - `npm init -y`
    - `npm add express`
    - `nano index.mjs`

`index.mjs`

```js
import express from 'express'
const server = express()
server.use('/', (request, response) => response.send('Hello, World!'))
server.listen(80, () => console.log(`Listening on :80`))
```

- Test the server app: `node --experimental-modules index.mjs` and go to [`http://31.31.77.33`](http://31.31.77.33)
- Create an NPM script for running in `package.json`: `nano package.json`:

`package.json`

```json
{
  "name": "root",
  "version": "1.0.0",
  "description": "",
  "main": "index.mjs",
  "scripts": {
    "start": "node --experimental-modules index.mjs",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "express": "^4.16.2"
  }
}
```

- Test the server app: `npm start` and go to [`http://31.31.77.33`](http://31.31.77.33)
- Handle crash recovery: `nano run.sh`

`run.sh`

```sh
#!/usr/bin/env bash
date | tee -a run.log
while :
do
    npm start | tee -a run.log
    date | tee -a run.log
done
```

- Make executable: `chmod +x run.sh`
- Run forever: `./run.sh &`
- Kill when needed:
    - `ps aux | grep run.sh` for hypervisor PID
    - `ps aux | grep index.mjs` for Node and app PIDs
    - `kill #` to kill hypervisor (prevent restart), Node and app
    - Kill only Node process to test out auto-restart

## Custom Domain

- Set up `A` and `AAAA` records to VPS IPv4 and IPv6 respectively for custom domain
- Wait for DNS record change to propagate
- Test the server app: [Hubelbauer.net](http://hubelbauer.net)

## Multiple Domains

- Configure multiple domains with the same VPS IPv4 and IPV6 DNS records (`A` and `AAAA`)
- [Install NGinx](http://nginx.org/en/linux_packages.html#stable)
 
```sh
curl -O http://nginx.org/keys/nginx_signing.key
sudo apt-key add nginx_signing.key
rm nginx_signing.key
nano /etc/apt/sources.list
```

`/etc/apt/sources.list`

```
# `codename` is distro name:
# 14.04 trusty
# 16.04 xenial
# 17.04 zesty
deb http://nginx.org/packages/ubuntu/ codename nginx
deb-src http://nginx.org/packages/ubuntu/ codename nginx
```

```sh
apt-get update
apt-get install nginx
```

- [Start NGinx](http://nginx.org/en/docs/beginners_guide.html): `nginx`
- Locate the configuration file: `nginx -t`
- Find the location of `.conf` directory in `nginx.conf`: `include /etc/nginx/conf.d/*.conf;`
- Configure NGinx to redirect to a given port depending on the domain:

`/etc/nginx/conf.d/virtual.conf`

```nginx
server {
    listen 80;
    server_name hubelbauer.net *.hubelbauer.net;
    location / {
        #proxy_pass http://localhost:8001;
        return 200 'hubelbauer.net';
        add_header Content-Type text/plain;
    }
}

server {
    listen 80;
    server_name async-await.net *.async-await.net;
    location / {
        #proxy_pass http://localhost:8002;
        return 200 'async-await.net';
        add_header Content-Type text/plain;
    }
}
```

- Test both domains:
  - [Hubelbauer.net](http://hubelbauer.net/)
  - [Async-Await.net](http://async-await.net/)
- Reload configuration: `nginx -s reload`

## Let's Encrypt

- Install Certbot:

```
add-apt-repository ppa:certbot/certbot
apt update
apt install python-certbot-nginx
```

- Obtain the certificate:
  - `certbot --authenticator standalone --installer nginx -d hubelbauer.net -d www.hubelbauer.net --pre-hook "nginx -s stop" --post-hook "nginx"`
  - `certbot --authenticator standalone --installer nginx -d async-await.net -d www.async-await.net --pre-hook "nginx -s stop" --post-hook "nginx"`

This should be just `certbot --nginx -d hubelbauer.net -d www.hubelbauer.net`, but [there is a security issue currently preventing it](https://github.com/certbot/certbot/issues/5405).

- Verify NGinx configuration changes applied by Certbot:

```nginx
server {
    listen 80;
    server_name hubelbauer.net *.hubelbauer.net;
    location / {
        proxy_pass http://localhost:8001;
    }

    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/hubelbauer.net/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/hubelbauer.net/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

    if ($scheme != "https") {
        return 301 https://$host$request_uri;
    } # managed by Certbot
}

server {
    listen 80;
    server_name async-await.net *.async-await.net;
    location / {
        proxy_pass http://localhost:8002;
    }

    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/async-await.net/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/async-await.net/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

    if ($scheme != "https") {
        return 301 https://$host$request_uri;
    } # managed by Certbot
}
```
